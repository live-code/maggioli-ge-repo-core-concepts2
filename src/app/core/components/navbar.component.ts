import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-navbar',
  template: `
    <nav class="navbar navbar-expand navbar-light bg-light fixed-top">
      <a class="navbar-brand" href="#">Navbar</a>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item" routerLinkActive="active">
            <a class="nav-link" routerLink="login">login </a>
          </li>
          <li class="nav-item" routerLinkActive="active">
            <a class="nav-link" routerLink="home">Home </a>
          </li>
          <li class="nav-item" routerLinkActive="active">
            <a class="nav-link" routerLink="catalog">Catalog</a>
          </li>
          <li class="nav-item" routerLinkActive="active">
            <a class="nav-link" routerLink="contacts">Contacts</a>
          </li>
        </ul>
      </div>
    </nav>
  `,
  styles: [`
  `]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
