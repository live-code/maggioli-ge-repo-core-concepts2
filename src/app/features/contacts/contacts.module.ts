import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsComponent } from './contacts.component';
import { CardComponent } from '../../shared/card.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ContactsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,

  ]
})
export class ContactsModule { }
