import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { News } from '../model/news';

@Component({
  selector: 'ma-news-list-item',
  template: `
      <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text></svg>
      <h2 class="super-title" [style.color]="news.color">{{news.title}}</h2>
      <p>{{news.desc}}</p>
      <p *ngIf="news.button"><a [href]="news.url" target="_blank" class="btn btn-secondary" role="button">{{news.button}}</a></p>
  `,
  styles: [`
   .super-title { font-size: 50px}
  `]
})
export class NewsListItemComponent  {
  @Input() news: News;
  // @HostBinding() class = 'col-lg-4';
}
