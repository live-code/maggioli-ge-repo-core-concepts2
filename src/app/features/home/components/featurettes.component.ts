import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ma-featurettes',
  template: `
    <div *ngFor="let featurette of featurettes; let i = index; let last = last; let odd = odd; let even = even">
      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7" [ngClass]="{'order-md-2': odd}">
          <h2 class="featurette-heading" [innerHTML]="(i+1) + ') ' + featurette.title"></h2>
          <p class="lead"> {{featurette.desc}}</p>
        </div>
        <div class="col-md-5" [ngClass]="{'order-md-1': odd}">
          <img [src]="featurette.image" [alt]="featurette.title" width="100%">
        </div>
      </div>

      <hr class="featurette-divider" *ngIf="last">
    </div>

  `,
  styles: [`
    .featurette-divider {
      margin: 5rem 0;
    }

    .featurette-heading {
      font-weight: 300;
      line-height: 1;
      letter-spacing: -.05rem;
    }

    @media (min-width: 40em) {
      .featurette-heading {
        font-size: 50px;
      }
    }

    @media (min-width: 62em) {
      .featurette-heading {
        margin-top: 7rem;
      }
    }

  `]
})
export class FeaturettesComponent implements OnInit {
  @Input() featurettes: any[];

  constructor() { }

  ngOnInit(): void {
  }

}
