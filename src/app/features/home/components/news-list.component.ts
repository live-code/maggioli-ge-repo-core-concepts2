import { Component, Input, OnInit } from '@angular/core';
import { News } from '../model/news';

@Component({
  selector: 'ma-news-list',
  template: `
    
      <!--....-->
      <div class="row">
        <ma-news-list-item
          class="col-lg-4"
          *ngFor="let news of newslist" [news]="news"></ma-news-list-item>
      </div>

      <!--....-->
  `,
  styles: [`
    .col-lg-4 {margin-bottom: 1.5rem;text-align: center;}
    h2 {font-weight: 400;}
    .col-lg-4 p {margin-right: .75rem;margin-left: .75rem;}
  `]
})
export class NewsListComponent {
  @Input() newslist: News[];

}
