import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ma-carousel',
  template: `
    <ngb-carousel *ngIf="items">
      <ng-template
        ngbSlide
        *ngFor="let item of items; let i = index "
      >
        <div class="picsum-img-wrapper">
          <img [src]="images[i]" alt="Random first slide">
        </div>
        <div class="carousel-caption">
          <h3>{{item.title}}</h3>
          <p>{{item.desc}}</p>
        </div>
      </ng-template>
    </ngb-carousel>
    
    <!--
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li
          *ngFor="let item of items; let index = index"
          (click)="currentIndex = index"
          [ngClass]="{active: currentIndex === index}"
        >
        </li>
      </ol>
      <div class="carousel-inner">
        <div
          *ngFor="let item of items; let index = index"
          class="carousel-item"
          [ngClass]="{active: currentIndex === index}"
        >
          <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"/></svg>
          <div class="container">
            <div class="carousel-caption text-right">
              <h1>{{item.title}}</h1>
              <p>{{item.desc}}</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
-->
  `,
  styles: [`

    /* Carousel base class */
    .carousel {
      margin-bottom: 4rem;
    }
    /* Since positioning the image, we need to help out the caption */
    .carousel-caption {
      bottom: 3rem;
      z-index: 10;
    }

    /* Declare heights because of positioning of img element */
    .carousel-item {
      height: 32rem;
    }
    .carousel-item > img {
      position: absolute;
      top: 0;
      left: 0;
      min-width: 100%;
      height: 32rem;
    }

    @media (min-width: 40em) {
      .carousel-caption p {
        margin-bottom: 1.25rem;
        font-size: 1.25rem;
        line-height: 1.4;
      }
    }
  `]
})
export class CarouselComponent implements OnInit {
  @Input() items: any[];

  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);


  currentIndex = 0;
  constructor() { }

  ngOnInit(): void {
  }

}
