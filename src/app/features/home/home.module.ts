import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { CarouselComponent } from './components/carousel.component';
import { FeaturettesComponent } from './components/featurettes.component';
import { NewsListComponent } from './components/news-list.component';
import { NewsListItemComponent } from './components/news-list-item.component';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HomeComponent,
    CarouselComponent,
    FeaturettesComponent,
    NewsListComponent,
    NewsListItemComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgbModule
  ]
})
export class HomeModule {}
