export interface News {
  title: string;
  desc: string;
  color?: string;
  button?: string;
  url?: string;
}
