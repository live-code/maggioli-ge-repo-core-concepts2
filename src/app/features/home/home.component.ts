import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-home',
  template: `
    <main role="main" [style.background]="data.config.bg">
      <ma-carousel [items]="data.carousel"></ma-carousel>
      <div class="container">
        <ma-news-list [newslist]="data.news"></ma-news-list>
        <ma-featurettes [featurettes]="data.featurettes"></ma-featurettes>
      </div>
    </main>
  `,
})
export class HomeComponent {
  data: any = {
    config: {
      bg: '#fefefe'
    },
    news: [
      { title: 'Heading 1', color: 'red', desc: '1 Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna', button: 'View Details', url: 'http://www.google.com'},
      { title: 'Heading 2', desc: '2 Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna', button: 'Go to site', url: 'http://www.google.com'},
      { title: 'Heading 3', desc: '3 Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna'},
    ],
    carousel: [
      { title: '1', desc: 'bla 1', align: 'right'},
      { title: '2', desc: 'bla 12'},
      { title: '3', desc: 'bla 123'},
      { title: '4', desc: 'bla 123'},
    ],
    featurettes: [
      {
        title: 'AAA featurette heading. <em>It’ll blow your mind.</em>',
        desc: '1 Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo',
        image: 'https://landezine.com/wp-content/uploads/2011/03/07_sant_en_co_landscape_architecture_De_Heuvel_Tilburg_square.jpg'
      },
      {
        title: 'BBB featurette heading. <em>It’ll blow your mind.</em>',
        desc: '2 Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo',
        image: 'https://landezine.com/wp-content/uploads/2011/03/07_sant_en_co_landscape_architecture_De_Heuvel_Tilburg_square.jpg'
      },
      {
        title: 'CC featurette heading. <em>It’ll blow your mind.</em>',
        desc: '3 Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo',
        image: 'https://landezine.com/wp-content/uploads/2011/03/07_sant_en_co_landscape_architecture_De_Heuvel_Tilburg_square.jpg'
      }
    ]
  }
}
