import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-catalog',
  template: `
    <p>
      catalog works!
      <input type="text" [ngModel]="label">
      <ma-card></ma-card>
    </p>

  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {
  label = 'ciao'
  constructor() { }

  ngOnInit(): void {
  }

}
