import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/components/navbar.component';
import { FooterComponent } from './core/components/footer.component';
import { ContactsModule } from './features/contacts/contacts.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContactsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
