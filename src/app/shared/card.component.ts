import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-card',
  template: `
    <div style="border: 1px dotted red">
      card works!
    </div>
  `,
  styles: [
  ]
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
