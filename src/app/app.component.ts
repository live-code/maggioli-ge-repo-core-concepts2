import { Component } from '@angular/core';

@Component({
  selector: 'ma-root',
  template: `
    <ma-navbar></ma-navbar>
    <router-outlet></router-outlet>
    <ma-footer></ma-footer>
  `,
})
export class AppComponent {
  title = 'maggioli-ge-repo-core-concepts2';
}
